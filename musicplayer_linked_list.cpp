#include <iostream>
#include<cstdlib>
//#include <conio.h>

using namespace std;


static int maxi=1;
class List{
	private:
		typedef struct node{
			string title;
			string singer;
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr tail;
	
	public:
		List();
		void AddNode(string addTitle, string addSinger);
		void DeleteNode(string delMusic);
		void PrintList();
		void EditNode(int index, string newMusic, string newSinger);
		int FindData(string music);
		void Playing(int index);
		
};

List::List(){
	head = NULL;
	curr = NULL;
	temp = NULL;
	tail = NULL;
}

void List::AddNode(string addTitle, string addSinger){
	nodePtr n = new node;
	n->next = NULL;
	n->title = addTitle;
	n->singer = addSinger;
	if(head != NULL){
		curr = head;
		while (curr->next != NULL){
			curr = curr->next;
		}
		curr->next = n;
		tail = n;
		maxi++;
	}
	else{
		head = n;
		tail = n;
		maxi++;
	}
}

void List::DeleteNode(string delMusic){
	nodePtr delPtr = NULL;
	temp = head;
	curr = head;
	while (curr != NULL && curr->title != delMusic){
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL){
		cout<<delMusic<<" was not in the List\n";
	}
	else if (curr==head){
		delPtr=head;
		head=head->next;
		maxi--;
	}
	else if(curr==tail){	
		tail = temp;
		maxi--;	
	}
	else{
		delPtr = curr;
		curr = curr->next;
		temp->next = curr;
		maxi--;
	}
	cout<< delMusic<<"Successfully Deleted!\n";
	delete delPtr;
}

void List::PrintList(){
	curr = head;
	int x = 1;
	//temp = curr->next;
	while (x!=maxi){
		cout<<"("<<x<<")\n";
		cout<<"Title: "<<curr->title<<"\n";
		cout<<"Singer: "<<curr->singer<<"\n";
		x++;
		curr = curr->next;
	}
}
//at a specified index
void List::EditNode(int index, string newMusic, string newSinger){
	curr = head;
	for (int currIndex = 1; currIndex <= index; currIndex++){
		curr = curr->next;
	}
	if (curr){
		curr->title = newMusic;
		curr->singer = newSinger;
	}
}

int List::FindData(string music){
	curr = head;
	int currIndex = 1;
	while (curr && curr->title != music){
		curr = curr->next;
		currIndex++;
	}
	if (curr){
		//cout<<music<<" is in the "<<currIndex<<"position"<<"\n";
		return currIndex;
	}
}
void List::Playing(int index){
	nodePtr n;
	curr = head;
	int currIndex = 1, maxicounter=1;
	while(curr&&curr->next!=NULL){
		curr=curr->next;
		maxicounter++;
	}
	if(index==1){
		curr=head;
		cout<<"Previous: None\n";
		while(currIndex!=index+1){
			temp = curr;
			curr=curr->next;
			currIndex++;
		}
		cout<<"Playing: "<<temp->title<<endl;
		cout<<"Next: "<<curr->title<<endl;
	}
	else if(index==maxicounter){
		curr=head;
		while (currIndex != index&&curr->next!=NULL){
			temp = curr;
			curr = curr->next;
			currIndex++;
		}
		cout<<"Previous: "<<temp->title<<endl;
		cout<<"Playing: "<<curr->title<<endl;
		cout<<"Next: None\n";
	}
	else{
		curr=head;
		while(currIndex!=index&&curr->next!=NULL){
			temp=curr;
			curr=curr->next;
			n=curr->next;
			currIndex++;
		}
		cout<<"Previous: "<<temp->title<<endl;
		cout<<"Playing: "<<curr->title<<endl;
		cout<<"Next: "<<n->title<<endl;
	}
}
bool Close(){
	return 0;
}
List yey;

void intro(){
	int c;
	string mtitle, singer;
	system("cls");
	cout<<" 1. Add Music\n 2. Display Playlist\n 3. Edit Music Title\n 4. Delete Music\n 5. Switch Off\n ";
	cin>>c;
	cin.ignore(1,'\n');
	if(c>5){
		cout<<"INVALID INPUT!\n\n";
		intro();
	}
	switch (c){
		case 1:{ 
			cout<<"Enter music title: "<<"\n";
			getline(cin, mtitle);
			cout<<"Enter singer: "<<"\n";
			getline(cin, singer);
			yey.AddNode(mtitle, singer);
			cout<<"Successfully Added!\n";
			system("pause");
			intro();
			break;
		}
		case 2:{
			int pick;
			yey.PrintList();
			cout<<"Choose a number to play: ";
			cin>>pick;
			cin.ignore(1,'\n');
			if(pick>maxi){
				cout<<"     INVALID INPUT! 	\n";
				intro();
			}
			yey.Playing(pick);
			system("pause");		
			intro();
			break;
		}
		case 3:{
			//edit
			string newTitle,newSinger;
			int edit;
			yey.PrintList();
			cout<<"Enter the number of the song to edit: \n";
			cin>>edit;
			cout<<"Enter the new title(thenpressEnter)\n";
			cout<<"Enter the new singer(thenpressEnter)\n";
			getline(cin, newTitle);
			getline(cin, newSinger);
			system("pause");
			yey.EditNode(edit-1, newTitle, newSinger);
			intro();
			break;
		}
		case 4:{
			//delete
			string mtitle;
			yey.PrintList();
			cout<<"Enter music title to delete:\n";
			getline(cin, mtitle);
			yey.DeleteNode(mtitle);
			intro();
			break;
		}
		case 5:{
			cout<<"                               GOODBYE.... ";
			Close();
			break;
		}
	}
}

int main(){
	yey.AddNode("Kay Tagal", "Mark Carpio");
	yey.AddNode("Walang Kwenta", "Bassilyo");
	yey.AddNode("Dungaw", "Gloc-9");
	yey.AddNode("Wonderful Sound", "Tom Jones");
	yey.AddNode("Crazy", "Kenny Rogers"); 
	yey.AddNode("Pulang Manok", "Unknown");
	yey.AddNode("Goodbye","Air Supply");
	yey.AddNode("Istokwa", "Lexus");   
	
	cout<<"           MUSIC PLAYER INITIALIZING...\n";
	for(int i=10; i<=100; i+=15){
		system("pause");
		if(i==100){
			system("cls");
			cout<<i<<"%\n";
			cout<<"OPENING...\n";
			system("pause");
			system("cls");
		}
		system("cls");
		cout<<i<<"%\n";
	}
	
	intro();
	return 0;
}
